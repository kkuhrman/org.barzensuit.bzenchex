/**
 * @file:	bzenchex.c
 * @brief:	main() for bzenchex CLI utilty.
 * 
 * @copyright:	Copyright (C) 2017 Kuhrman Technology Solutions LLC
 * @license:	GPLv3+: GNU GPL version 3
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

/* gnulib */
#include "closeout.h"
#include "getopt.h"
#include "long-options.h"
#include "progname.h"

/* libbzenc */
#include "bzenapi.h"

/*bzenchex package */
#include "bzenchex_cmd.h" /* OK */
#include "bzenchex_intl.h" /* OK */
#include "bzenchex_help.h" /* OK */
#include "bzenchex_log.h"
#include "bzenchex.h" /* OK */

static const struct option longopts[] = {
  {"help", optional_argument, NULL, 'h'},
  {"version", optional_argument, NULL, 'v'},
  {NULL, 0, NULL, 0}
};

int main (int argc, char *argv[])
{
  int optc;
  const char *user_cmd;
  int result;

  /* Set program name. */
  set_program_name (argv[0]);

  /* Set locale via LC_ALL.  */
  setlocale (LC_ALL, "");

#if ENABLE_NLS
  /* Set the text message domain.  */
  bindtextdomain (PACKAGE, LOCALEDIR);
  textdomain (PACKAGE);
#endif

  /* Even exiting has subtleties.  On exit, if any writes failed, change
     the exit status.  The /dev/full device on GNU/Linux can be used for
     testing; for instance, bzenchex >/dev/full should exit unsuccessfully.
     This is implemented in the Gnulib module "closeout".  */
  atexit (bzenchex_closeout);

  /* @todo: if this cannot be option it must come from /etc/conf. */
  bzenchex_log_severity_level_set(BZENLOG_DEBUG);

  while ((optc = getopt_long (argc, argv, "h:v:", longopts, NULL)) != -1)
    switch (optc)
      {
	/* --help and --version exit immediately, per GNU coding standards.  */
      case 'v':
        bzenchex_print_version(optarg);
	exit(EXIT_SUCCESS);
	break;
      case 'h':
        bzenchex_print_help(optarg);
	exit(EXIT_SUCCESS);
	break;
      default:
        if (optind >= argc)
	  {
            bzenchex_print_help(NULL);
	    exit(EXIT_SUCCESS);
          }
	break;
      }

    /* Process command. */
    user_cmd = argv[optind];
    result = bzenchex_cmd_handler(user_cmd);

  exit (result);
}


/* Make sure open files, sockets, logging etc. are closed properly on shutdown. */
void bzenchex_closeout()
{
  bzenchex_log_shutdown();
  close_stdout;
}
