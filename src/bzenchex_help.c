/**
 * @file:	bzenchex_help.c
 * @brief:	Implement --help and --version on command line.
 * 
 * @copyright:	Copyright (C) 2017 Kuhrman Technology Solutions LLC
 * @license:	GPLv3+: GNU GPL version 3
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include <stdio.h>

/* gnulib */
#include "progname.h"

/* libbzenc */
#include "bzenapi.h"

/*bzenchex package */
#include "bzenchex.h"
#include "bzenchex_cmd.h"
#include "bzenchex_intl.h"
#include "bzenchex_log.h"
#include "bzenchex_help.h"


/* Display program help on stdout. */
void bzenchex_print_help(const char* cmd)
{
  char msgbuf[BZENCHEX_MAX_MESSAGE_SIZE];
  int cmd_code;
  int status;

  /* For debugging purposes. */
  snprintf(msgbuf, 
	   BZENCHEX_MAX_MESSAGE_SIZE, 
	   "bzenchex_print_help(%s)", 
	   cmd);
  status = bzen_log_write(BZENCHEX_LOG_DEBUG, 
			  BZENLOG_DEBUG,
			  msgbuf);

  if (cmd != NULL)
    {
      /* Get numeric code for comand. */
      cmd_code = bzenchex_cmd_code(cmd);
    }
  else
    {
      cmd_code = 0;
    }

  /* Return help display corresponding to option/arg */
  switch (cmd_code)
    {
    default:
      bzenchex_print_help_default();
      break;
    case BZENCHEX_CMD_STATUS:
      bzenchex_print_help_cmd_status();
      break;
    }
}

/* Display default help message. */
void bzenchex_print_help_default()
{
  /*  This long message is split into several pieces to help translators be able
      to align different blocks and identify the various pieces.  */

  /* TRANSLATORS: --help output 1 (synopsis)
     no-wrap */
  printf (_("\
Usage: %s [OPTION] [COMMAND]\n"), program_name);

  /* TRANSLATORS: --help output 2 (brief description)
     no-wrap */
  fputs (_("\
\nbzenchex provides an overview of Project Barzensuit software \
\n\installed on the local machine. \
\n\t1. Which project components are installed? \
\n\t2. Which schema of bzenmp does a component comply with? \
\n\t3. Which bzenmp transactions does a component support? \
\n\t4. Which components are loaded (running in the background)? \
\n\t5. What is the state of a component (started/stopped/error)? \
\n\t6. What is the version of an installed project package? \
\n\nFor more information about the Barzensuit Project see https://www.barzensuit.org.\n\n"), stdout);

  /* repeat this two fn() sequence for more text to follow */
  puts ("");
  /* TRANSLATORS: --help output 3: options 1/2
     no-wrap */
  fputs (_("\
  -h, --help          display this help and exit\n\
  -v, --version       display version information and exit\n"), stdout);
  
  bzenchex_print_help_trailer();
}

/* Display help for 'status' command. */
void bzenchex_print_help_cmd_status()
{
    /* TRANSLATORS: --help output 1 (synopsis)
     no-wrap */
  printf (_("\
Usage: %s [OPTION] status [COMPONENT]\n"), program_name);

  bzenchex_print_help_trailer();
}

/* Standard text at end of every help message. */
void bzenchex_print_help_trailer()
{
  printf ("\n");
  /* TRANSLATORS: --help output 4+ (reports)
     TRANSLATORS: the placeholder indicates the bug-reporting address
     for this application.  Please add _another line_ with the
     address for translation bugs.
     no-wrap */
  printf (_("\
Report bugs to: %s\n"), PACKAGE_BUGREPORT);
#ifdef PACKAGE_PACKAGER_BUG_REPORTS
  printf (_("Report %s bugs to: %s\n"), PACKAGE_PACKAGER,
	  PACKAGE_PACKAGER_BUG_REPORTS);
#endif
#ifdef PACKAGE_URL
  printf (_("%s home page: <%s>\n"), PACKAGE_NAME, PACKAGE_URL);
#else
  printf (_("%s home page: <http://www.gnu.org/software/%s/>\n"),
	  PACKAGE_NAME, PACKAGE);
#endif
  fputs (_("General help using GNU software: <http://www.gnu.org/gethelp/>\n"),
	 stdout);
}

/* Display program version info on stdout. */
void bzenchex_print_version(const char* package)
{
  char msgbuf[BZENCHEX_MAX_MESSAGE_SIZE];
  int status;
  
  /* For debugging purposes. */
  snprintf(msgbuf, 
	   BZENCHEX_MAX_MESSAGE_SIZE, 
	   "bzenchex_print_version(%s)", 
	   package);
  status = bzen_log_write(BZENCHEX_LOG_DEBUG, 
			  BZENLOG_DEBUG,
			  msgbuf);

  printf ("%s (%s) %s\n", PACKAGE, PACKAGE_NAME, VERSION);
  puts ("");

  printf (_("\
Copyright (C) %d %s.\n\
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>\n\
This is free software: you are free to change and redistribute it.\n\
There is NO WARRANTY, to the extent permitted by law.\n"),
	  BZENCHEX_COPYRIGHT_YEAR, BZENCHEX_COPYRIGHT_HOLDER);
}



