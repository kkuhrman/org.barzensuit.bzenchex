/**
 * @file:	bzenchex_status.c
 * @brief:	Handler routines for user command 'status'.
 * 
 * @copyright:	Copyright (C) 2017 Kuhrman Technology Solutions LLC
 * @license:	GPLv3+: GNU GPL version 3
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include <stdio.h>

/* gnulib */

/* libbzenc */
#include "bzenapi.h"

/*bzenchex package */
#include "bzenchex.h"
#include "bzenchex_intl.h"
#include "bzenchex_log.h"
#include "bzenchex_status.h"

const char* hc_msg = "GET / HTTP/1.1 \n \
  Host: server.example.com\n \
  Connection: Upgrade, HTTP2-Settings\n \
  Upgrade: h2c\n \
  HTTP2-Settings: <base64url encoding of HTTP/2 SETTINGS payload>";

/**
 * Main handler for user command 'status'.
 *
 * @return int 0 on success, -1 on error.
 */
int bzenchex_cmd_status()
{
  /* socket variables */
  int client_socket_fd;
  struct sockaddr_in* server_address;
  size_t server_address_size;
  int namespace = PF_INET;
  int style = SOCK_STREAM;
  int protocol = 0;
  uint32_t host;
  uint32_t port;
  int format;

  /* messaging and logging variables */
  char msgbuf[BZENCHEX_MAX_MESSAGE_SIZE];
  size_t msglen;
  size_t bytect;

  /* status and error variables */
  int cmd_code;
  int status;
  int result;

  /* For debugging purposes. */
  snprintf(msgbuf, 
	   BZENCHEX_MAX_MESSAGE_SIZE, 
	   "CoR passes to bzenchex_cmd_status()");
  status = bzen_log_write(BZENCHEX_LOG_DEBUG, 
			  BZENLOG_DEBUG,
			  msgbuf);

  /* construct server address */
  host = INADDR_LOOPBACK;
  port = 7100; /* @todo: export bzend.h to /usr/local/include/bzend/ */
  server_address =   bzen_socket_address_in(host, port);
  server_address_size = sizeof(*server_address);
  if (server_address == NULL)
    {
      snprintf(msgbuf, 
	       BZENCHEX_MAX_MESSAGE_SIZE, 
	       "%s failed to open Inet address on local host port %d.",
	       PACKAGE_NAME, port);
      status = bzen_log_write(BZENCHEX_LOG_ERRORS, 
			      BZENLOG_ERROR,
			      msgbuf);
    }

  /* create client socket */
  client_socket_fd = bzen_socket_open(namespace, style, protocol);
  if (client_socket_fd < 0)
    {
      snprintf(msgbuf, 
	       BZENCHEX_MAX_MESSAGE_SIZE, 
	       "%s failed to open Inet socket.",
	       PACKAGE_NAME);
      status = bzen_log_write(BZENCHEX_LOG_ERRORS, 
			      BZENLOG_ERROR,
			      msgbuf);
    }

  /* request connection with server  */
  status = bzen_socket_connect(client_socket_fd, 
			       (struct sockaddr*)server_address, 
			       server_address_size);
  if (status < 0)
    {
      snprintf(msgbuf, 
	       BZENCHEX_MAX_MESSAGE_SIZE, 
	       "%s failed to connect with bzend mq component on port %d.",
	       PACKAGE_NAME, port);
      status = bzen_log_write(BZENCHEX_LOG_ERRORS, 
			      BZENLOG_ERROR,
			      msgbuf);
    }

  /* Send HTTP/2 hc upgrade request. */
  msglen = sizeof(char) * strlen(hc_msg);
  bytect = bzen_socket_send(client_socket_fd, hc_msg, msglen, 0);
  /* check bytes sent */
  /* For debugging purposes. */
  snprintf(msgbuf, 
	   BZENCHEX_MAX_MESSAGE_SIZE, 
	   "%d of %d bytes sent.", 
	   bytect, msglen);
  status = bzen_log_write(BZENCHEX_LOG_DEBUG, 
			  BZENLOG_DEBUG,
			  msgbuf);

  status = bzen_socket_close(client_socket_fd, SHUT_RDWR);
  if (status != 0)
    {
      snprintf(msgbuf, 
	       BZENCHEX_MAX_MESSAGE_SIZE, 
	       "%s failed to close Inet socket %d.",
	       PACKAGE_NAME, client_socket_fd);
      status = bzen_log_write(BZENCHEX_LOG_ERRORS, 
			      BZENLOG_ERROR,
			      msgbuf);
    }
  

  result = EXIT_SUCCESS;

  return result;
}
