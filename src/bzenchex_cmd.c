/**
 * @file:	bzenchex_cmd.c
 * @brief:	Manages symbolic & numerical representation of program commands.
 * 
 * @copyright:	Copyright (C) 2017 Kuhrman Technology Solutions LLC
 * @license:	GPLv3+: GNU GPL version 3
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include "string.h"

/* bzenchex package */
#include "bzenchex.h"
#include "bzenchex_cmd.h"
#include "bzenchex_help.h"
#include "bzenchex_status.h"

static const char* BZENCHEX_CMD_STR[BZENCHEX_CMD_ALL] = 
  {
    "null",
    "status"
  };

/* Returns numeric representation of a text command. */
int bzenchex_cmd_code(const char* cmd)
{
  int result;
  int cmd_code;

  result = -1;
  for(cmd_code = BZENCHEX_CMD_NULL; cmd_code < BZENCHEX_CMD_ALL; cmd_code++)
    {
      if (0 == strcmp(cmd, BZENCHEX_CMD_STR[cmd_code]))
	{
	  result = cmd_code;
	  break;
	}
    }
  return result;
}

/* Route user comand to corresponding handler function. */
int bzenchex_cmd_handler(const char* cmd)
{
  int cmd_code;
  int result;

  if (cmd != NULL)
    {
      /* Get numeric code for comand. */
      cmd_code = bzenchex_cmd_code(cmd);
    }
  else
    {
      cmd_code = 0;
    }

  /* Return help display corresponding to option/arg */
  switch (cmd_code)
    {
    default:
      bzenchex_print_help_default();
      result = EXIT_SUCCESS;
      break;
    case BZENCHEX_CMD_STATUS:
      result = bzenchex_cmd_status();
      break;
    }

  return result;
}
