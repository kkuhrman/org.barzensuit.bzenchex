/**
 * @file:       bzenchex_intl.h
 * @brief:	Support for internationalization vis-à-vis Gnu gettext.
 * 
 * @copyright:	Copyright (C) 2017 Kuhrman Technology Solutions LLC
 * @license:	GPLv3+: GNU GPL version 3
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _BZENCHEX_INTLTEXT_H_
#define _BZENCHEX_INTLTEXT_H_

#include <config.h>
#include <locale.h>

/* Unicode. */
#include <wchar.h>
#include <wctype.h>

/* Gnu gettext */
# include "gettext.h"
# define _(str) gettext (str)
# define N_(str) gettext_noop (str)

#endif /* _BZENCHEX_INTLTEXT_H_ */
