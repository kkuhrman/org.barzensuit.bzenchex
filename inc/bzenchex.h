/**
 * @file:	bzenchex.h
 * @brief:	Global declarations, includes and defines for bzenchex package.
 * 
 * bzenchex provides a comprehensize template for how an end user program should
 * interact with the Barzensuit Service Protocol (BSP) to answer the 
 * following questions:
 * 1. What project components are installed on this machine?
 * 2. Which schema of bzenmp does a component comply with?
 * 3. Which bzenmp transactions does a component support?
 * 4. Which components are loaded (running in the background)?
 * 5. What is the state of a component (started/stopped/error)?
 * 6. What is the version of an installaed project package?
 *file:///home/karl/Documents/KTS/barzensuit.org/software/projects/org.barzensuit.chex/README.md

 * Apart from these queries, bzenchex provides a decent template for end user 
 * program design.  It is itself an adpation of Gnu Hello, which models Gnu 
 * coding standards, Gnu maintainer practices, the Gnu Portability Library, 
 * internationaliztion by way of Gnu gettext and  an excellent introduction 
 * to autotools and the Gnu Build System.
 *
 * @see:	https://www.gnu.org/software/hello/
 * @see:	https://www.gnu.org/prep/standards/
 * @see:	https://www.gnu.org/prep/maintain/
 * @see:	https://www.gnu.org/software/automake/manual/
 * @see:        
 *
 * For more information about Project Barzensuit @see the documentation 
 * @ https://www.barzensuit.org.
 *
 * @copyright:	Copyright (C) 2017 Kuhrman Technology Solutions LLC
 * @license:	GPLv3+: GNU GPL version 3
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _BZENCHEX_H_
#define _BZENCHEX_H_

#include <config.h>
#include <errno.h>
#include <error.h>
#include <stdlib.h>

/**
 * Make sure open files, sockets, logging etc. are closed properly on shutdown.
 */
void bzenchex_closeout();

#endif /* _BZENCHEX_H_ */
