/**
 * @file:	bzenchex_cmd.h
 * @brief:	Manages symbolic & numerical representation of program commands.
 * 
 * @copyright:	Copyright (C) 2017 Kuhrman Technology Solutions LLC
 * @license:	GPLv3+: GNU GPL version 3
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _BZENCHEX_CMD_H_
#define _BZENCHEX_CMD_H_

#include <config.h>

enum BZENCHEX_CMD 
  {
    BZENCHEX_CMD_NULL = 0,
    BZENCHEX_CMD_STATUS,
    BZENCHEX_CMD_ALL
  };

/**
 * Returns numeric representation of a text command.
 *
 * @param const char* cmd Text command.
 *
 * @return int Numeric code of text command or -1 if not exists.
 */
int bzenchex_cmd_code(const char* cmd);

/**
 * Route user comand to corresponding handler function.
 *
 * @param const char* cmd Text command.
 *
 * @return int Return (exit) code.
 */
int bzenchex_cmd_handler(const char* cmd);

#endif /* _BZENCHEX_CMD_H_ */
